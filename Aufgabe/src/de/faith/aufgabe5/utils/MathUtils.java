package de.faith.aufgabe5.utils;

public class MathUtils {

    /*
    * @param number die Zahl die gerundet werden soll
    * @param positionsBehind wie viele Nachkommastellen die Zahl haben soll
    * @return gibt die gerundete Zahl zurück
    * */
    public static double round(double number, int positionsBehind) {
        return ((int) number + (Math.round(Math.pow(10, positionsBehind) * (number - (int) number))) / (Math.pow(10, positionsBehind)));
    }

}
