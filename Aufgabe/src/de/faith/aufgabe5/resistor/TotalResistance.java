package de.faith.aufgabe5.resistor;

import de.faith.aufgabe5.utils.MathUtils;

public class TotalResistance {

    private Resistor totalResistance;
    private Resistor totalResistance2;

    public void setTotalResistance(Resistor totalResistance) {
        this.totalResistance = totalResistance;
    }

    public void setTotalResistance(Resistor totalResistance, Resistor totalResistance2) {
        this.totalResistance = totalResistance;
        this.totalResistance2 = totalResistance2;
    }

    //@return gibt den Gesamtwiderstand zurück
    public double getTotalResistor() {
        return MathUtils.round(((totalResistance2 != null) ? (totalResistance.getTotalResistor() + totalResistance2.getTotalResistor()) : totalResistance.getTotalResistor()), 3);
    }

    //@return gibt den Bauplanzurück
    public String getBluePrint() {
        return ((totalResistance2 != null) ? totalResistance.getBluePrint() + " + " + totalResistance2.getBluePrint() : totalResistance.getBluePrint());
    }
}