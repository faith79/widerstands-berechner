package de.faith.aufgabe5.resistor;

import de.faith.aufgabe5.utils.MathUtils;

public class Resistor {

    private final double[] resistors;
    private final boolean series;

    /*
    @param series Soll eine Reihen oder Parallelschaltung erstellt werden.
    @param resistors Array von Widerständen aus dem das Netzwerk erzeugt wird.
     */
    public Resistor(boolean series, double[] resistors) {
        this.series = series;
        this.resistors = resistors;
    }

    /*
     * Berechnet die Widerstände zu einer Reihen oder Parallelschaltung
     * @return gibt das gerundete Ergebnis zurück mit 3 Nachkommastellen
     */
    private double calculateTotalResistor() {
        double totalResistor = 0;

        for (double r1 : resistors) {
            if (series) {
                totalResistor += r1;
            }
        }
        if (!series) {
            if (resistors.length >= 2) {
                totalResistor = 1 / (1 / resistors[0] + 1 / resistors[1] + (resistors.length > 2 ? (1 / resistors[2]) : 0) + (resistors.length > 3 ? (1 / resistors[3]) : 0));
            }
        }
        return MathUtils.round(totalResistor, 3);
    }

    public double[] getResistors() {
        return resistors;
    }

    public double getTotalResistor() {
        return calculateTotalResistor();
    }

    public boolean isSeries() {
        return series;
    }

    // Gibt den Bauplan für das Netzwerk zurück
    public String getBluePrint() {
        String resistorsToString = "";
        for (int i = 0; i < resistors.length; i++) {
            double resistor = resistors[i];
            if (series) {
                resistorsToString += resistor + (i == resistors.length - 1 ? " Î© " : " Î© + ");
            } else {
                if (i == 0) {
                    resistorsToString = "(";
                }
                resistorsToString += resistor + (i == resistors.length - 1 ? " Î©)" : " Î© || ");
            }
        }
        return resistorsToString;
    }

}
